const getUserList = async (req, res) => {
    try {
        let filters = {};
        const key = req.params.key;
        if (key) {
            filters = {
                $or: [
                    {"first_name": {$regex: key, $options: "i"}},
                    {"last_name": {$regex: key, $options: "i"}},
                    {"email": {$regex: key, $options: "i"}}
                ]
            };
        }
        let users = await User.find(filters).exec();

        const results = [];

        let j = 1;
        for (let i = 0; i < 1; i++) {
            for(const user of users) {

                results.push({
                    ...user.toJSON(),
                    num: j
                });

                j++;
            }
        }

        res.json(results);
    } catch(e) {
        res.json(e);
    }
};

const postUserCreate = (req, res) => {
    const first_name = req.body.first_name;
    const last_name = req.body.last_name;
    const email = req.body.email;
    const age = req.body.age;

    const u = new User({
        first_name: first_name,
        last_name: last_name,
        email: email,
        age: age
    });

    u.save();

    io.emit("users", {
        type: "[Users] Create",
        payload: u
    });
    toast("New User created !!!", u.first_name + ' ' + u.last_name, 'success');
    res.json({success: true, msg: "User Created"});
};

const deleteUser = async (req, res) => {
    const user_id = req.query.user_id;
    const deletedUser = await User.findByIdAndRemove(user_id).exec();

    if (!deletedUser) {
        return res.json({msg: "User not found"});
    }

    io.emit("users", {
        type: "[Users] Delete",
        payload: deletedUser._id
    });
    toast("User deleted !!!", deletedUser.first_name + ' ' + deletedUser.last_name, 'error');
    res.json({msg: deletedUser.first_name + " Deleted successfully"});
};

const putUpdateUser = async (req, res) => {
    const user_id = req.body.user_id;

    const fields = [
        "first_name",
        "last_name",
        "email",
        "age"
    ];

    const updateObj = {};
    for(field of fields) {
        if(req.body[field]) {
            updateObj[field] = req.body[field];
        }
    }

    const updatedUser = await User.findByIdAndUpdate(user_id, updateObj).exec();
    res.json({msg: "User Updates successfully", updatedUser});
};

const putUpdateUserSort = async (req, res) => {
    const sorting = req.body.sorting;

    for (const user_id in sorting) {
        await User.updateOne({_id: user_id}, {sort: sorting[user_id]}, {upsert: true}).exec();
    }

    io.emit("users", {
        type: "[Users] Load"
    });
    toast("Users list updated", "sorting updated", "info");
    res.json({msg: "Users Updated successfully"});
};


const getUserListWithPaging = async (req, res) => {

    const queryOptions = {
        page: parseInt(req.query.page),
        limit: parseInt(req.query.limit),
    };

    if (req.query.sort) {
        queryOptions['sort'] = {
            [req.query.sort]: req.query.reverse === "true" ? 'desc' : 'asc'
        };
    }

    console.log(queryOptions);

    const users = await User.paginate({}, queryOptions);

    res.json(users);
};

module.exports = {
    getUserList,
    postUserCreate,
    deleteUser,
    putUpdateUser,
    putUpdateUserSort,
    getUserListWithPaging
};