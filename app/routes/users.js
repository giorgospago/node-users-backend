const express = require('express');
const router = express.Router();
const UsersController = require('../controllers/UsersController');


router.get("/pagination", UsersController.getUserListWithPaging);

router.get("/:key?", UsersController.getUserList);
router.post("/create", UsersController.postUserCreate);
router.delete("/delete", UsersController.deleteUser);
router.put("/update", UsersController.putUpdateUser);
router.put("/update-sort", UsersController.putUpdateUserSort);

module.exports = router;