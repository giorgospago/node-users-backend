const mongoose = require("mongoose");
const mongoosePaginate = require('mongoose-paginate-v2');

const userSchema = mongoose.Schema({
    first_name: String,
    last_name: String,
    email: String,
    age: Number,
    sort: {type: Number, default: 0},
});

userSchema.plugin(mongoosePaginate);

const User = mongoose.model('User', userSchema);

module.exports = User;