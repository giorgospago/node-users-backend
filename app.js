const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const express = require("express");
const cors = require("cors");

// Initialize Express Application
const app = express();

const server = require('http').createServer(app);

// Initialize Web Sockets
global.io = require('socket.io')(server);

io.on("connection", socket => {
    console.log(socket.id, " connected");

    setTimeout(() => {
        socket.emit("users", {msg: "Welcome to Users Socket"});
    }, 5000);
});



global.toast = function(title, message, method) {
    io.emit("toast", {
        type: "[Toast] show",
        payload: {
            method: method,
            title: title,
            message: message
        }
    });
};






// Connect to Database
const con = mongoose.connect("mongodb://5.189.177.98:37017/PagonoudisUsers", { useNewUrlParser: true });

// Listen to port 3000
server.listen(3000);


// Define Database Models
global.User = require('./app/models/User');
const MailController = require('./app/controllers/MailController');


// Middlewares
app.use(cors());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

// Routes
app.get("/", (req, res) => {
    res.send("Hello world !!");
});

app.get("/mail", MailController.sendRandomMail);

app.use("/users", require("./app/routes/users"));

app.post("/toast", (req, res) => {
    toast(req.body.title, req.body.message, req.body.method);

    res.json({success: true});
});
